// Copyright Epic Games, Inc. All Rights Reserved.

#include "HeyAndTeammate.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, HeyAndTeammate, "HeyAndTeammate" );

DEFINE_LOG_CATEGORY(LogHeyAndTeammate)
 